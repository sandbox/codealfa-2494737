<?php

/**
 * JCH Optimize - Aggregate and minify external resources for optmized downloads
 * 
 * @author Samuel Marshall <sdmarshall73@gmail.com>
 * @copyright Copyright (c) 2010 Samuel Marshall
 * @license GNU/GPLv3, See LICENSE file
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If LICENSE file missing, see <http://www.gnu.org/licenses/>.
 */

$DIR = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));

define('DRUPAL_ROOT', $DIR);
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';

drupal_bootstrap(DRUPAL_BOOTSTRAP_CONFIGURATION);

require_once DRUPAL_ROOT . '/includes/cache.inc';

foreach (variable_get('cache_backends', array()) as $include)
{
        require_once DRUPAL_ROOT . '/' . $include;
}

require_once DRUPAL_ROOT . '/includes/database/database.inc';

spl_autoload_register('drupal_autoload_class');
spl_autoload_register('drupal_autoload_interface');

drupal_load('module', 'jch_optimize');

JchOptimizeOutput::getCombinedFile();

