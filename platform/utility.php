<?php

/**
 * JCH Optimize - Aggregate and minify external resources for optmized downloads
 * 
 * @author Samuel Marshall <sdmarshall73@gmail.com>
 * @copyright Copyright (c) 2010 Samuel Marshall
 * @license GNU/GPLv3, See LICENSE file
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If LICENSE file missing, see <http://www.gnu.org/licenses/>.
 */

class JchPlatformUtility implements JchInterfaceUtility
{
        /**
         * 
         * @param type $path
         */
        public static function createFolder($path)
        {
                return drupal_mkdir($path, NULL, TRUE);
        }

        /**
         * 
         * @param type $value
         * @param type $default
         * @param type $filter
         * @param type $method
         */
        public static function get($value, $default = '', $filter = 'cmd', $method = 'request')
        {
                switch ($filter)
                {
                        case 'int':
                                $filter = FILTER_SANITIZE_NUMBER_INT;

                                break;

                        case 'string':
                        case 'cmd':
                        default :
                                $filter = FILTER_SANITIZE_STRING;

                                break;
                }

                switch ($method)
                {
                        case 'get':
                                $type = INPUT_GET;

                                break;

                        case 'post':
                                $type = INPUT_POST;

                                break;

                        default:

                                if (!isset($_REQUEST[$value]))
                                {
                                        $_REQUEST[$value] = $default;
                                }

                                return filter_var($_REQUEST[$value], $filter);
                }


                $input = filter_input($type, $value, $filter);

                return is_null($input) ? $default : $input;
        }

        /**
         * 
         */
        public static function getLogsPath()
        {
                return '\'Admin -> Reports -> Recent log messages\'';
        }

        /**
         * 
         */
        public static function isMsieLT10()
        {
                if (isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
                {
                        preg_match('#MSIE ([0-9.]+)#', $_SERVER['HTTP_USER_AGENT'], $version);

                        if ((int) $version[1] < 10)
                        {
                                return TRUE;
                        }
                }
                
                return FALSE;
        }

        /**
         * 
         * @return string
         */
        public static function lnEnd()
        {
                return "\n";
        }

        /**
         * 
         * @param type $message
         * @param type $priority
         * @param type $filename
         */
        public static function log($message, $priority, $filename)
        {
                watchdog('jch_optimize', $message, array(), constant('WATCHDOG_' . $priority));
        }

        /**
         * 
         * @return string
         */
        public static function tab()
        {
                return "\t";
        }

        /**
         * 
         * @param type $text
         * @return type
         */
        public static function translate($text)
        {
                return t($text);
        }

        /**
         * 
         */
        public static function unixCurrentDate()
        {
                return time();
        }

        /**
         * 
         * @param type $value
         * @return type
         */
        public static function decrypt($value)
        {
                return self::encrypt_decrypt($value, 'decrypt');
        }

        /**
         * 
         * @param type $value
         * @return type
         */
        public static function encrypt($value)
        {
                return self::encrypt_decrypt($value, 'encrypt');
        }

        /**
         * 
         * @param type $value
         * @param type $action
         * @return type
         */
        private static function encrypt_decrypt($value, $action)
        {
                global $drupal_hash_salt;

                $output = false;

                $encrypt_method = "AES-256-CBC";
                $secret_key     = $drupal_hash_salt;
                $secret_iv      = $drupal_hash_salt;

                // hash
                $key = hash('sha256', $secret_key);

                // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
                $iv = substr(hash('sha256', $secret_iv), 0, 16);

                if ($action == 'encrypt')
                {
                        $output = openssl_encrypt($value, $encrypt_method, $key, 0, $iv);
                        $output = base64_encode($output);
                }
                else if ($action == 'decrypt')
                {
                        $output = openssl_decrypt(base64_decode($value), $encrypt_method, $key, 0, $iv);
                }

                return $output;
        }

        /**
         * 
         * @return type
         */
        public static function menuId()
        {
                return current_path();
        }

}
