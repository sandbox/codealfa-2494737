<?php

/**
 * JCH Optimize - Aggregate and minify external resources for optmized downloads
 * 
 * @author Samuel Marshall <sdmarshall73@gmail.com>
 * @copyright Copyright (c) 2010 Samuel Marshall
 * @license GNU/GPLv3, See LICENSE file
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If LICENSE file missing, see <http://www.gnu.org/licenses/>.
 */

class JchPlatformCache implements JchInterfaceCache
{

        /**
         * 
         * @param type $id
         * @param type $lifetime
         * @return type
         */
        public static function getCache($id, $lifetime)
        {
                $cache = cache_get($id, 'cache_jch_optimize');
                
                if($cache === FALSE)
                {
                        return FALSE;
                }
                
                return $cache->data;
        }

        /**
         * 
         * @param type $id
         * @param type $lifetime
         * @param type $function
         * @param type $args
         * @return type
         */
        public static function getCallbackCache($id, $lifetime, $function, $args)
        {
                $cache = cache_get($id, 'cache_jch_optimize');
                
                if ($cache === FALSE)
                {
                        $contents = call_user_func_array($function, $args);
                        
                        $expire = strtotime('+' . $lifetime . ' seconds');
                        cache_set($id, $contents, 'cache_jch_optimize', $expire);

                        return $contents;

                }

                return $cache->data;
        }
}
