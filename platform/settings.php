<?php

/**
 * JCH Optimize - Aggregate and minify external resources for optmized downloads
 * 
 * @author Samuel Marshall <sdmarshall73@gmail.com>
 * @copyright Copyright (c) 2010 Samuel Marshall
 * @license GNU/GPLv3, See LICENSE file
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If LICENSE file missing, see <http://www.gnu.org/licenses/>.
 */

class JchPlatformSettings implements JchInterfaceSettings
{

        protected $params          = array();
        protected static $instance = NULL;

        /**
         * 
         * @param type $params
         * @return \JchOptimizeSettings
         */
        public static function getInstance($params)
        {
                if (is_null(self::$instance))
                {
                        self::$instance = new JchPlatformSettings($params);
                }
                
                return self::$instance;
        }

        /**
         * 
         * @param type $param
         * @param type $default
         * @return type
         */
        public function get($param, $default = NULL)
        {
                if (!isset($this->params['jch_optimize_' . $param]))
                {
                        return $default;
                }

                return $this->params['jch_optimize_' . $param];
        }

        /**
         * 
         * @param type $params
         */
        private function __construct($params)
        {
                if (!defined('JCH_DEBUG'))
                {
                        define('JCH_DEBUG', ($this->get('debug', 0)));
                }

                global $conf;

                foreach ($conf as $key => $value)
                {
                        if (preg_match('#^jch_optimize_#', $key))
                        {
                                $this->params[$key] = $value;
                        }
                }
        }

        /**
         * 
         * @param type $param
         * @param type $value
         */
        public function set($param, $value)
        {
                $this->params['jch_optimize_' . $param] = $value;
        }

        /**
         * 
         */
        public function getOptions()
        {
                return $this->params;
        }

}
