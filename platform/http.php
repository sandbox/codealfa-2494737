<?php

/**
 * JCH Optimize - Aggregate and minify external resources for optmized downloads
 * 
 * @author Samuel Marshall <sdmarshall73@gmail.com>
 * @copyright Copyright (c) 2010 Samuel Marshall
 * @license GNU/GPLv3, See LICENSE file
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If LICENSE file missing, see <http://www.gnu.org/licenses/>.
 */

class JchPlatformHttp implements JchInterfaceHttp
{
        protected static $instance;
        
        public function available()
        {
                $works = variable_get('drupal_http_request_fails', NULL);
                
                if (is_null($works))
                {
                        return system_check_http_request();
                }
                
                return !$works;
        }

        public function request($sPath, $aPost=null, $aHeaders=null)
        {
                $args = array();

                $args['timeout'] = 5;
                
                if (!is_null($aPost))
                {
                        $args['data'] = http_build_query($aPost);
                }
                
                $response = drupal_http_request($sPath, $args);

                $return = array(
                        'body' => isset($response->data) ? $response->data : '',
                        'code' => isset($response->code) ? $response->code : 0
                );

                return $return;
        }

        public static function getHttpAdapter()
        {
               if (!self::$instance)
                {
                        self::$instance = new JchPlatformHttp();
                }

                return self::$instance; 
        }

}