<?php

/**
 * JCH Optimize - Aggregate and minify external resources for optmized downloads
 * 
 * @author Samuel Marshall <sdmarshall73@gmail.com>
 * @copyright Copyright (c) 2010 Samuel Marshall
 * @license GNU/GPLv3, See LICENSE file
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * If LICENSE file missing, see <http://www.gnu.org/licenses/>.
 */

class JchPlatformPaths implements JchInterfacePaths
{

        /**
         * 
         * @param type $url
         * @return type
         */
        public static function absolutePath($url)
        {
                return str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, DRUPAL_ROOT) . DIRECTORY_SEPARATOR .
                        str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $url);
        }

        /**
         * 
         * @param type $function
         */
        public static function ajaxUrl($function)
        {
                $options = array(
                        'absolute' => TRUE
                );

                return url('jchoptimize/ajax/' . $function, $options);
        }

        /**
         * 
         * @return type
         */
        public static function assetPath()
        {
                return self::rewriteBase() . 'jch_optimize/assets';
        }

        /**
         * 
         * @return type
         */
        public static function imageFolder()
        {
                return JCH_PLUGIN_URL . '/media/images/';
        }

        /**
         * 
         * @param type $sPath
         */
        public static function path2Url($sPath)
        {
                $oUri        = clone JchPlatformUri::getInstance();
                $sBaseFolder = JchOptimizeHelper::getBaseFolder();

                $abspath = str_replace(DIRECTORY_SEPARATOR, '/', DRUPAL_ROOT . '/');
                $sPath   = str_replace(DIRECTORY_SEPARATOR, '/', $sPath);

                $sUriPath = $oUri->toString(array('scheme', 'user', 'pass', 'host', 'port')) . $sBaseFolder .
                        (str_replace($abspath, '', $sPath));

                return $sUriPath;
        }

        /**
         * 
         * @staticvar string $rewrite_base
         * @return string
         */
        public static function rewriteBase()
        {
                static $rewrite_base;

                if (!isset($rewrite_base))
                {
                        $module_folder = preg_replace('#jch_optimize$#', '', JCH_PLUGIN_URL);
                        $rewrite_base  = JchOptimizeHelper::getBaseFolder() . $module_folder;
                }

                return $rewrite_base;
        }

        /**
         * 
         * @return type
         */
        public static function rootPath()
        {
                return DRUPAL_ROOT . DIRECTORY_SEPARATOR;
        }

        /**
         * 
         * @param type $url
         * @return type
         */
        public static function spriteDir($url = FALSE)
        {
                $sprite_path = conf_path() . '/files/jch_optimize/sprites';

                if ($url)
                {
                        return JchOptimizeHelper::getBaseFolder() . $sprite_path . '/';
                }

                return DRUPAL_ROOT . '/' . $sprite_path;
        }

}
